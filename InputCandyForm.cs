﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class InputCandyForm : Form
    {
        public Candy InputCandy { get; private set; }
        public InputCandyForm()
        {
            InitializeComponent();
            InputCandy = null;
            DialogResult = DialogResult.None;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            bool isCorrect = tbxName.Text != "" && tbxWeight.Text != "" && tbxPrice.Text != "" &&
                      tbxManufacturer.Text != "" && tbxShelfLife.Text != "";
            if (!isCorrect)
            {
                InputCandy = null;
                DialogResult = DialogResult.No;
            }
            else
            {
                Read();
                DialogResult = DialogResult.Yes;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (InputCandy == null)
                InputCandy = new Candy();
            InputCandy.GenerateRandom();
            Display();
        }

        void Display()
        {
            if (InputCandy != null)
            {
                tbxName.Text = InputCandy.Name;
                tbxWeight.Text = InputCandy.Weight.ToString();
                tbxPrice.Text = InputCandy.Price.ToString();
                tbxManufacturer.Text = InputCandy.Manufacturer;
                dtpDate.Value = InputCandy.DateOfManufacture;
                tbxShelfLife.Text = InputCandy.ShelfLife.ToString();
            }
        }

        void Read()
        {
            InputCandy = new Candy();
            InputCandy.Name = tbxName.Text;
            InputCandy.Weight = Convert.ToInt32(tbxWeight.Text);
            InputCandy.Price = Convert.ToInt32(tbxPrice.Text);
            InputCandy.Manufacturer = tbxManufacturer.Text;
            InputCandy.DateOfManufacture = dtpDate.Value;
            InputCandy.ShelfLife = Convert.ToInt32(tbxShelfLife.Text);
        }

   

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar));
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar));
        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar));
        }

        private void InputCandyForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.None)
                DialogResult = DialogResult.Cancel;
        }
    }
}
