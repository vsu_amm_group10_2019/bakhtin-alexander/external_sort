﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Candy : IComparable
    {
        private static Random rnd = new Random();
        public string Name { get; set; }
        public int Weight { get; set; }
        public int Price { get; set; }
        public string Manufacturer { get; set; }
        public DateTime DateOfManufacture { get; set; }
        public int ShelfLife { get; set; }

        public Candy()
        {
            Name = "—";
            Weight = 0;
            Price = 0;
            Manufacturer = "—";
            DateOfManufacture = DateTime.Parse("2020.01.01");
            ShelfLife = 0;
        }
        public Candy(string information)
        {
            string[] info = information.Split();
            info = info.Where(x => x != "").ToArray();
            Name = info[0];
            Weight = Convert.ToInt32(info[1]);
            Price = Convert.ToInt32(info[2]);
            Manufacturer = info[3];
            DateOfManufacture = DateTime.Parse(info[4]);
            ShelfLife = Convert.ToInt32(info[5]);
        }

        public void GenerateRandom()
        {
            Name = GenerateString();
            Weight = 1 + rnd.Next(1000);
            Price = 1 + rnd.Next(1000);
            Manufacturer = GenerateString();
            DateOfManufacture = GenerateDate();
            ShelfLife = 1 + rnd.Next(1000);
        }

        public override string ToString()
        {
            return Name.PadRight(9) + Weight.ToString().PadRight(5) + Price.ToString().PadRight(5) +
                Manufacturer.PadRight(9) + DateOfManufacture.ToShortDateString().PadRight(11) + 
                ShelfLife.ToString();
        }

        public int CompareTo(object obj)
        {
            Candy candy = (Candy)(obj);
            return Name.CompareTo(candy.Name);
        }

        private string GenerateString()
        {
            int size = rnd.Next(8);
            string result = "";
            result += (char)('A' + rnd.Next(26));
            for (int i = 0; i < size; i++)
                result += (char)('a' + rnd.Next(26));
            return result;
        }

        private DateTime GenerateDate()
        {
            DateTime date = new DateTime(2021, 1, 1);
            return date.AddDays(rnd.Next(500));
        }
    }
}
