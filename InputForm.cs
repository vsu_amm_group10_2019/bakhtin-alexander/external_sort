﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class InputForm : Form
    {
        public InputForm()
        {
            InitializeComponent();
            DialogResult = DialogResult.None;
        }

        public string fileName
        {
            get { return textBox1.Text; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool isCorrectFileName = fileName != "1.txt" && fileName != "2.txt" && fileName != "" &&
                                     fileName != "3.txt" && fileName != "4.txt";
            if (isCorrectFileName)
                DialogResult = DialogResult.OK;
            else
                DialogResult = DialogResult.No;
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void InputForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.None)
                DialogResult = DialogResult.Cancel;
        }

 
    }
}
