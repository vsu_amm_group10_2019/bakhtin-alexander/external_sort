﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string InputFileName = "";

        // естественное двухпутевое однофазное слияние
        void MergeSort(string inputfileName, string outputFileName)
        {
            string[] fileNames = new string[] { "1.txt", "2.txt", "3.txt", "4.txt" };
            for (int j = 0; j < 4; j++) // создаем вспомогательные файлы
            {
                StreamWriter f = new StreamWriter(fileNames[j]);
                f.Close();
            }
            // переливаем входной файл по двум вспомогательным.
            // файл с именем fileNames[2] гарантированно пуст
            // если перелили в один файл, то получили отсортированный файл
            bool isSorted = MergeSerieses(inputfileName, fileNames[2], fileNames[0], fileNames[1]);
            int resultIndex = 0; // индекс названия файла, содержащего результат
            while (!isSorted) // пока файл не отсортирован
            {
                if (resultIndex == 0) // если нечетный номер переливания, льем из 1 и 2 в 3 и 4
                    isSorted = MergeSerieses(fileNames[0], fileNames[1], fileNames[2], fileNames[3]);                    
                else // иначе из 3 и 4 в 1 и 2
                    isSorted = MergeSerieses(fileNames[2], fileNames[3], fileNames[0], fileNames[1]);
                resultIndex += 2;
                resultIndex %= 4;
            }
            File.Copy(fileNames[resultIndex], outputFileName, true); // сохраняем результат
            for (int j = 0; j < 4; j++) // удаляем вспомогательные файлы
                File.Delete(fileNames[j]);
        }


        // переливаем содержимое двух вспомогательных файлов в другие два
        bool MergeSerieses(string f1_in, string f2_in, string f1_out, string f2_out)
        {
            StreamReader f1_read = new StreamReader(f1_in);
            StreamReader f2_read = new StreamReader(f2_in);
            StreamWriter[] f_write = new StreamWriter[] { new StreamWriter(f1_out), new StreamWriter(f2_out) };
         
            Candy lastRead1 = null;
            Candy lastRead2 = null;
            string info = f1_read.ReadLine();
            if (info != null)
                lastRead1 = new Candy(info);
            info = f2_read.ReadLine();
            if (info != null)
                lastRead2 = new Candy(info);

            int i = 0; // количество получившихся серий
            // пока хотя бы 1 файл не пуст — сливаем серии в новый файл
            while (lastRead1 != null || lastRead2 != null)
            {
                // сливаем крайние серии в 1,
                // чередуем файлы, в которые сливаем
                Merge(f1_read, f2_read, f_write[i % 2], ref lastRead1, ref lastRead2);
                i++; 
            }

            f1_read.Close();
            f2_read.Close();
            f_write[0].Close();
            f_write[1].Close();
            return i <= 1;
        }

        /// <summary>
        /// сливаем крайнии серии из файлов
        /// </summary>
        /// <param name="f1_in"> первый файл, содержащий серию </param>
        /// <param name="f2_in"> второй файл, содержащий серию </param>
        /// <param name="f_out"> файл, в который сливаются серии </param>
        /// <param name="lastRead1"> последний считанный из файла fr1 элемент </param>
        /// <param name="lastRead2"> последний считанный из файла fr2 элемент </param>
        void Merge(StreamReader f1_in, StreamReader f2_in, StreamWriter f_out, ref Candy lastRead1, ref Candy lastRead2)
        {
            bool isSeriesF1 = lastRead1 != null; // файл содержит серию, если он не пуст
            bool isSeriesF2 = lastRead2 != null;
            while(isSeriesF1 && isSeriesF2) // пока что оба файла сожержат серии
            {
                if (lastRead1.CompareTo(lastRead2) <= 0) // элемент первого файла меньше второго
                    isSeriesF1 = AddFromFile(f1_in, f_out, ref lastRead1);
                else
                    isSeriesF2 = AddFromFile(f2_in, f_out, ref lastRead2);
            }
            if (isSeriesF1) // если в первом файле серия не закончилась
                while (AddFromFile(f1_in, f_out, ref lastRead1)); // добавляем из него элементы, пока идет серия
            else if (isSeriesF2) // если во втором файле серия не закончилась
                while(AddFromFile(f2_in, f_out, ref lastRead2)); // добавляем из него элементы, пока идет серия
        }

        /// <summary>
        /// записываем в файл последний считанный элемент
        /// </summary>
        /// <param name="f_in"> файл, из которого считан lastRead</param>
        /// <param name="f_out"> файл, в который записываем lastRead</param>
        /// <param name="lastRead"> последний считанный элемент из файла </param>
        /// <returns> возвращаем true, если в файле, из которого считали элемент, продолжается текущая серия </returns>
        bool AddFromFile(StreamReader f_in, StreamWriter f_out, ref Candy lastRead)
        {
            f_out.WriteLine(lastRead); // записываем последний считанный элемент в файл
            Candy lastWritten = lastRead; // запоминаем последний записанный элемент
            string info = f_in.ReadLine(); // считывает следующий элемент из файла file_read
            // если элемент отсутсвует, то в info будет записан null
            if (info == null)
            {
                lastRead = null;
                return false; // возвращаем false, т.к. серия заканчивается вместе с файлом
            }
            // иначе запоминаем последний считанный элемент
            lastRead = new Candy(info);
            // если последний считанный > или >= предыдущего, вернется true, 
            // т.к. в файле все еще идет необходимая серия
            // иначе вернется false, т.к. серия закончилась
            return lastRead.CompareTo(lastWritten) >= 0;
        }

        private void отсортироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (InputFileName == "")
                MessageBox.Show("Необходимо открыть файл.", "Уведомление");
            else
            {
                using (InputForm input = new InputForm())
                {
                    if (input.ShowDialog() != DialogResult.OK)
                        MessageBox.Show("Некорректное имя файла. Файл не создан.", "Уведомление");
                    else
                    {
                        if (!File.Exists(input.fileName) ||
                            MessageBox.Show("Файл существует. Перезаписать?", "Запрос",
                            MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            MergeSort(InputFileName, input.fileName);
                            MessageBox.Show("Файл отортирован.", "Уведомление");
                            richTextBox1.LoadFile(InputFileName, RichTextBoxStreamType.PlainText);
                            richTextBox2.LoadFile(input.fileName, RichTextBoxStreamType.PlainText);
                        }
                        else
                            MessageBox.Show("Отмена создания файла для сортировки.", "Уведомление");
                    }
                }
            }

        }


        private void создатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string caption = "Уведомление";
            using (InputForm input = new InputForm())
            {
                DialogResult result = input.ShowDialog();
                if (result == DialogResult.Cancel)
                    MessageBox.Show("Отмена создания файла.", caption);
                else if (result == DialogResult.No)
                    MessageBox.Show("Некорректное имя файла. Файл не создан.", caption);
                else if (!File.Exists(input.fileName) ||
                         MessageBox.Show("Файл существует. Перезаписать?", "Запрос",
                         MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    StreamWriter newFile = new StreamWriter(input.fileName);
                    newFile.Close();
                    MessageBox.Show("Файл создан.", caption);
                }
                else
                    MessageBox.Show("Отмена создания файла.", caption);
            }
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string caption = "Уведомление";
            using (InputForm input = new InputForm())
            {
                DialogResult result = input.ShowDialog();
                if (result == DialogResult.Cancel)
                    MessageBox.Show("Отмена создания файла.", caption);
                else if (result == DialogResult.No)
                    MessageBox.Show("Некорректное имя файла. Файл не открыт.", caption);
                else if (!File.Exists(input.fileName))
                    MessageBox.Show("Файл не существует.", caption);
                else
                {
                    InputFileName = input.fileName;
                    richTextBox1.LoadFile(InputFileName, RichTextBoxStreamType.PlainText);
                    richTextBox2.Text = "";
                    MessageBox.Show("Файл открыт.", caption);
                }
            }
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string caption = "Уведомление";
            if (InputFileName != "")
            {
                richTextBox1.SaveFile(InputFileName, RichTextBoxStreamType.PlainText);
                MessageBox.Show("Файл сохранён.", caption);
            }
            else
                MessageBox.Show("Файл не сохранён, т.к. он не открыт.", caption);
        }

        private void добавитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string caption = "Уведомление";
            if (InputFileName == "")
            {
                MessageBox.Show("Необходимо открыть файл.", caption);
                return;
            }

            using (InputCandyForm input = new InputCandyForm())
            {
                DialogResult result = input.ShowDialog();
                if (result == DialogResult.Cancel)
                    MessageBox.Show("Отмена добавления элемента.", caption);
                else if (result == DialogResult.No)
                    MessageBox.Show("Некорректное значение элемента. Элемент не добавлен.", caption);
                else
                {
                    Candy candy = input.InputCandy;
                    richTextBox1.Text += candy.ToString() + Environment.NewLine;
                    MessageBox.Show("Элемент добавлен.", caption);
                }
            }
        }
    }
}
